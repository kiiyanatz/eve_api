MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_USERNAME = ''
MONGO_PASSWORD = ''
MONGO_DBNAME = 'admin'


# Enable CRUD for api endpoints
RESOURCE_METHODS = ['GET', 'POST', 'DELETE']

# Enalbe CRUD for items
ITEM_METHODS = ['GET', 'PATCH', 'PUT', 'DELETE']


# Tasks schema definition
schema = {
	'title': {
		'type': 'string',
		'minlength': 1,
		'maxlength': 100,
	},
	'description': {
		'type': 'string',
		'minlength': 1,
		'maxlength': 400,
	},
	'importance': {
		'type': 'list',
		'allowed': ["urgent", "immediate", "later"],
	},
	'due': {
		'type': 'datetime',
	},
}

tasks = {
	'item_title': 'task',
	'additional_lookup': {
		'url': 'regex("[\w]+")',
		'field': 'title'
	},
	'cache_control': 'max-age=10,must-revalidate',
	'cache_expires': 10,

	'resource_methods': ['GET', 'POST'],
	'schema': schema
}

DOMAIN = {
	'tasks': tasks,
}